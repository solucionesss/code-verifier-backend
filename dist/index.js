"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
// Configuration archivo dotenv 
dotenv_1.default.config();
// Crear Express APP 
const app = (0, express_1.default)();
const port = process.env.PORT || 8000;
/*
dsadasd
dasdasd
dasdas
*/
//Define the first route of APP
app.get('/', (req, res) => {
    // send Hello World
    res.send('Welcome to My API Restful  APP Express + TS + Modemon + Jess + Swagger + Mongoose - By SSS');
});
app.get('/hello', (req, res) => {
    // send Hello World
    res.send('Welcome to GET Route:  !Hello World');
});
// Execute the APP - Listen request to PORT
app.listen(port, () => {
    console.log(`EXPRESS SERVER: running at http://localhost:${port}`);
});
//# sourceMappingURL=index.js.map