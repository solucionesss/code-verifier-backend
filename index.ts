import express, { Express, Request, Response, response } from "express";
import dotenv from 'dotenv';

// Configuration archivo dotenv 
dotenv.config();

// Crear Express APP 
const app: Express = express();
const port: string | number = process.env.PORT || 8000;

/*
dsadasd
dasdasd
dasdas
*/

//Define the first route of APP
app.get ('/', (req: Request,res: Response) => {
    // send Hello World
    res.send ('Welcome to My API Restful  APP Express + TS + Modemon + Jess + Swagger + Mongoose - By SSS');
});

app.get ('/hello', (req: Request,res: Response) => {
    // send Hello World
    res.send ('Welcome to GET Route:  !Hello World');
});

// Execute the APP - Listen request to PORT
app.listen (port, () => {
    console.log (`EXPRESS SERVER: running at http://localhost:${port}`)
} )